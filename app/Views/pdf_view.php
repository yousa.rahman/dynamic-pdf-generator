<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>PDF Generator</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
  <div class="container mt-5">

    <h2>Generate PDF</h2>

    <table class="table table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Contact</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <form method="post" >
                <td><input type="text" name="name" required></td>
                <td><input type="email" name="email" required></td>
                <td><input type="number" name="contact" ></td>
                <td><input type="submit" class="btn btn-primary btn-sm" value="Add"></td>
            </form>
        </tr>
      </tbody>
    </table>
    <br>
  </div>
</body>

</html>
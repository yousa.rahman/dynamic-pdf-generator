
<div class="container">

<div class="d-flex flex-row-reverse bd-highlight">
  <a href="{htmlToPDF_url}" class="btn btn-primary">
    Generate PDF 
  </a>
</div>
  <br>
  
    <table class="table table-striped table-hover mt-4">
      <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Contact</th>
        <th></th>
      </tr>
       
      </thead>
      {result}
        <tbody>
        <tr>
          <td>{name}</td>
          <td>{email}</td>
          <td>{contact}</td>
          <td><button class="btn-danger btn-sm">Delete</button></td>
        </tr>
        </tbody>
      {/result}  
    </table>  
  </div>
</body>

</html>
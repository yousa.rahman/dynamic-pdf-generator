<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\PdfModel;
use App\Models\Model;

class PdfController extends Controller
{

    public function index() 
	{
        $parser = \Config\Services::parser();

        
        $db = db_connect();
		$model = new Model($db);
		$result[] = $model->all();
		// echo '<pre>';
		// 	print_r($result[0]);
		// echo '<pre>';

       
        // $data = [
        //     'subjectList' => [
        //        [ 'Subject' => 'Maths' , 'Teacher' => 'Mosh' , 'Age' => 'Balalal'],
        //        [ 'Subject' => 'Science' , 'Teacher' => 'Master', 'Age' => 'Balalal'],
        //        [ 'Subject' => 'Biology' , 'Teacher' => 'Joshua','Age' => 'Balalal']
        //     ]
        // ];
        echo view('pdf_view');
        if($this->request->getMethod() == 'post'){
            $model = new PdfModel();
            $model->save($_POST);          
            
        }
        $parser->setData(['result'=>$result[0],'htmlToPDF_url'=>base_url('Pdfcontroller/htmlToPDF')]);
        return $parser->render('pdf_view2');
             
    }

    function htmlToPDF(){
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('pdf_view2'));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }

}
<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

class Model{

    protected $db;

    public function __construct(ConnectionInterface &$db){
        $this->db =& $db;
    }

    function all(){
        return $this->db->table('info')->get()->getResult();
    }

}